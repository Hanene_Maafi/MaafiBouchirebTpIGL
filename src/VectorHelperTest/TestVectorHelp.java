package VectorHelperTest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestVectorHelp {

	protected VectorHelp test;
	protected VectorHelp testFixe;

	@Before
	public void setUp() throws Exception {
		Vector v = new Vector(new int[] {4,10,8,3},4);
		
		this.test = new VectorHelp(v);
Vector v1 = new Vector(new int[] {4,10,8,3},4);
		
		this.testFixe = new VectorHelp(v1);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testVectorHelper() {
		fail("Not yet implemented");
	}

	@Test
	public void testTrierVecteur() throws VecteurNulException {
		final int[] vecteurTrie = {3,4,8,10};
		this.test.trierVecteur(this.test.getVecteur());
		Assert.assertArrayEquals(vecteurTrie,this.test.getVecteur().getTab() );
	}

	@Test
	public void testSomme() throws TailleDifferenteException, VecteurNulException {
		final int[] vecteurSomme = {8,20,16,6};
		Assert.assertArrayEquals(vecteurSomme, (this.testFixe.somme(this.testFixe.getVecteur(), this.testFixe.getVecteur()).getTab()));
		
	
	}

	@Test
	public void testAjouterNombre() throws VecteurNulException {
		final int[] vecteurRes = {8,14,12,7};
		this.testFixe.ajouterNombre(this.testFixe.getVecteur(), 4);
		Assert.assertArrayEquals(vecteurRes, this.testFixe.getVecteur().getTab());
	}

	@Test
	public void testVectorMaxMin() throws VecteurNulException {
		final int Max =10,Min=3;
		this.test.vectorMaxMin(this.test.getVecteur());
		Assert.assertEquals(Max, this.test.getVecteur().getMax());
		Assert.assertEquals(Min, this.test.getVecteur().getMin());
	}

	@Test
	public void testInverserElementsVecteur() {
		
		final int [] vecteurInver={4,3,2,1};
		this.testFixe.setVecteur(new Vector(new int[] {1,2,3,4},4));
		
		Assert.assertArrayEquals(vecteurInver,this.testFixe.inverserElementsVecteur(this.testFixe.getVecteur()).getTab());

	}

}
