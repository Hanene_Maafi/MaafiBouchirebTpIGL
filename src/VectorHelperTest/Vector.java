package VectorHelperTest;

/**
 * @author MAAFII
 * 
 *
 */
public class Vector {
	private int[] tab ;
	/**
	 * L'if nest ps modifiable
	 * 
	 * @see Vector#setTailleVecteur()
	 *@see Vector#getTailleVecteur()
	 */
	private int tailleVecteur;
	/**
	 * L'if nest ps modifiable
	 * 
	 *
	 */
	private int max = -1;
	private int min = -1;
	
	public Vector(int[] tab, int tailleVecteur) {
		
		this.tab = tab;
		this.tailleVecteur = tailleVecteur;
	}
	
	public Vector( int tailleVecteur) {
		
		this.tab = new int[tailleVecteur];
		this.tailleVecteur = tailleVecteur;
		for (int i=0 ; i<tailleVecteur; i++) this.tab[i] = 0;
	}
	
	public Vector( int tailleVecteur, int x) {
		super();
		this.tab = new int[this.tailleVecteur];
		this.tailleVecteur = tailleVecteur;
		for (int i=0 ; i<tailleVecteur; i++) this.tab[i] = x ;
	} 
	
	public int[] getTab() 
	{
		return tab;
	}
	
	public void setTab(int[] tab) 
	{
		this.tab = tab;
	}
	
	public void setTab() 
	{
		
	}
	public int getTabElement(int indice) 
	{
		return tab[indice];
	}
	
	public void setTabElement(int indice,int element) 
	{
		 tab[indice] = element;
	}
	
	public int getTailleVecteur()
	{
		return tailleVecteur;
	}
	
	
	public void affiche ()
	{ 
		int i=0;
		for (i=0;i<this.tailleVecteur;i++)
		{
			System.out.println (this.getTabElement(i)+" ") ;
		
		
		}
		
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

}
