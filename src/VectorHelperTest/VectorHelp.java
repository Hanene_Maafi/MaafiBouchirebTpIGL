package VectorHelperTest;

public class VectorHelp {
    private Vector vecteur;


    public VectorHelp(Vector vecteur) {
        super();
        this.vecteur = vecteur;
    }

    public Vector getVecteur() {
        return vecteur;
    }

    public void setVecteur(Vector vecteur) {
        this.vecteur = vecteur;
    }

    /**
	 * ************ Cette fonction  permet de trie un tableaux donne en entree en utlilisant le principe de tri en bulle
	 * 
	 * ********** A la fin de l procedure le tableau sera trie
	 * @param v
	 * @throws VecteurNulException
	 */
	
    public void trierVecteur(Vector v) throws VecteurNulException {
        if (v == null) {
            throw new VecteurNulException();

        }
        int taille = v.getTailleVecteur();
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < (taille - 1); j++) {
                if (v.getTabElement(j + 1) < v.getTabElement(j)) {
                    v.setTabElement(j, v.getTabElement(j) + v.getTabElement(j + 1));
                    v.setTabElement(j + 1, v.getTabElement(j) - v.getTabElement(j + 1));
                    v.setTabElement(j, v.getTabElement(j) - v.getTabElement(j + 1));
                }

            }
        }
    }

    /**
	 * ************  Cette fonction  permet de sommer les elements de deux tableaux donnes en entree deux par deux
	 * 
	 * ************ Elle retourne en sortie le r�sultat de la somme
	 * @param v1
	 * @param v2
	 * @return
	 * @throws TailleDifferenteException
	 * @throws VecteurNulException
	 */

    public Vector somme(Vector v1, Vector v2) throws TailleDifferenteException, VecteurNulException {
        if ((v1 == null) || (v2 == null)) {
            throw new VecteurNulException();
        }
        if (v1.getTailleVecteur() != v2.getTailleVecteur()) {
            throw new TailleDifferenteException();
        }
        int n = v1.getTailleVecteur();
        Vector res = new Vector(n);
        for (int i = 0; i < n; i++) {
            res.setTabElement(i, v1.getTabElement(i) + v2.getTabElement(i));
        }
        return res;
    }
    /**
	 * ************ Cette fonction  permet d'ajouter un nombre donne par l'utilisateur  aux �l�ments du tableau
	 * @param v
	 * @param nombre
	 * @throws VecteurNulException
	 */

    public void ajouterNombre(Vector v, int nombre) throws VecteurNulException {
        if (v == null) {
            throw new VecteurNulException();

        }
        for (int i = 0; i < v.getTailleVecteur(); i++) v.setTabElement(i, (v.getTabElement(i) + nombre));
    }
    /**
	 *  * ************  Cette fonction  permet de rechercher les bornes d'un tableau (maximale et minimale)
     *
     * **********Le MAX et le MIN pourront etre consulter apres l"appel de la procedure dans en utilsant les getters de ses deux parametre
 	 * @param v
	 * @throws VecteurNulException
	 */

    public void vectorMaxMin(Vector v) throws VecteurNulException {

        if (v == null) {
            throw new VecteurNulException();
        }
        int taille = v.getTailleVecteur();
        int max = v.getTabElement(0);
        int min = v.getTabElement(0);
        for (int i = 0; i < taille; i++) {
            if (max < v.getTabElement(i)) {
                max = v.getTabElement(i);

            }
            if (v.getTabElement(i) < min) {
                min = v.getTabElement(i);


            }

        }
        v.setMax(max);
        v.setMin(min);

    }

    /**
	 * * ************  Cette fonction  permet d'inverser les �l�ments d'un tableau
	 * @param v
	 */

    public Vector inverserElementsVecteur(Vector v) {
        int i = 0,j=0;
        
        int taille = v.getTailleVecteur();
        
        Vector inver = new Vector ( taille) ;
        
        for (i = (taille - 1); i >= 0; i--) {
        
        	//System.out.println(" " + v.getTabElement(i));
            inver.setTabElement(j, v.getTabElement(i));
            j++;
        }
        return inver;

    }


}
