package VectorHelperTest;

import org.junit.Test;

import static org.junit.Assert.*;

public class VectorHelpTest {

	
	protected VectorHelp test;
	protected VectorHelp testFixe;

	@Before
	public void setUp() throws Exception {
		Vector v = new Vector(new int[] {4,10,8,3},4);
		
		this.test = new VectorHelp();
Vector v1 = new Vector(new int[] {4,10,8,3},4);
		
		this.testFixe = new VectorHelp(v1);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testVectorHelper() {
		fail("Not yet implemented");
	}

	@Test
	public void testTrierVecteur() throws VecteurNulException {
		final int[] vecteurTrie = {3,4,8,10};
		this.test.trierVecteur(this.test.getVecteur());
		Assert.assertArrayEquals(vecteurTrie,this.test.getVecteur().getTab() );
	}

	@Test
	public void testSomme() throws TailleDifferenteException, VecteurNulException {
		final int[] vecteurSomme = {8,20,16,6};
		Assert.assertArrayEquals(vecteurSomme, (this.testFixe.somme(this.testFixe.getVecteur(), this.testFixe.getVecteur()).getTab()));
		
	
	}

	@Test
	public void testAjouterNombre() throws VecteurNulException {
		final int[] vecteurRes = {8,14,12,7};
		this.testFixe.ajouterNombre(this.testFixe.getVecteur(), 4);
		Assert.assertArrayEquals(vecteurRes, this.testFixe.getVecteur().getTab());
	}

	@Test
	public void testVectorMaxMin() {
		fail("Not yet implemented");
	}

	@Test
	public void testInverserElementsVecteur() {
		fail("Not yet implemented");
	}

}